/* Prologue  -*- C++ -*- */

%option noyywrap noinput nounput

%{
#include "parse-calc.hh"
yy::parser::location_type loc;

#define YY_USER_ACTION                          \
    loc.columns(yyleng);
%}

%%
  loc.step();

 // Rules.
"+"     return yy::parser::make_PLUS(loc);
"-"     return yy::parser::make_MINUS(loc);
"*"     return yy::parser::make_STAR(loc);
"/"     return yy::parser::make_SLASH(loc);
"("     return yy::parser::make_LPAREN(loc);
")"     return yy::parser::make_RPAREN(loc);
[0-9]+  return yy::parser::make_NUMBER(strtol(yytext, nullptr, 0), loc);

[ \t]+  loc.step(); continue;
\n      loc.lines(yyleng); return yy::parser::make_EOL(loc);

.       {
  std::cerr << "unexpected character: " << yytext << '\n';
  num_errors += 1;
}

<<EOF>>  return yy::parser::make_EOF(loc);
%%
// Epilogue.
