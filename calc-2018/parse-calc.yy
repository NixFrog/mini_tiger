// Prologue  -*- C++ -*-

// Generate C++.
%language "C++"

// Don't use unions, but variants, so that we can put objects in the
// stack.
%define api.value.type variant

// Require for make_FOO type-safe functions for all the token types.
%define api.token.constructor

// Prefix all the tokens in the implementation, to avoid clashes
// with, for instance, #define EOF.
%define api.token.prefix {TOK_}

// Verbose error messages.
%define parse.error verbose

// Enable yydebug.
%define parse.trace

// Enable location tracking.
%locations

// Let the parser "return" the last result.
%parse-param { int& result }

// Let the parser (and the scanner) exchange an argument with its
// caller: the number of errors.
%param { int& num_errors }

// Issue these declaration in the header file, after the definition
// of yy::parser.
%code provides
{
  // The declaration, for the scanner.
#define YY_DECL yy::parser::symbol_type yylex(int& num_errors)

  // The declaration, for the parser.
  YY_DECL;
}

// Our tokens.
%token <int> NUMBER "number"
%token PLUS "+"
       MINUS "-"
       STAR "*"
       SLASH "/"
       LPAREN "("
       RPAREN ")"
%token EOF 0 "end-of-file"
%token EOL "end-of-line"

// Our typed non terminals.
%nterm <int> exp line

// Better debug traces.
%printer { yyo << $$; } <int>;

// Precedence/associativity.
%left "+" "-"
%left "*" "/"

%%
// Rules.
input:
  %empty
| input line    { result = $2; }
;

line:
  exp EOL       { $$ = $1; std::cout << $1 << '\n'; }
| error EOL     { $$ = 666; }
;

exp:
  exp "+" exp   { $$ = $1 + $3; }
| exp "-" exp   { $$ = $1 - $3; }
| exp "*" exp   { $$ = $1 * $3; }
| exp "/" exp   { $$ = $1 / $3; }
| "(" exp ")"   { $$ = $2; }
| "(" error ")" { $$ = 666; yyerrok; }
| NUMBER        { $$ = $1; }
| "+" exp       { $$ = $2; }
| "-" exp       { $$ = -$2; }
;
%%
// Epilogue.
void yy::parser::error(const location_type& loc, const std::string& s)
{
  num_errors += 1;
  std::cerr << loc << ": " << s << '\n';
}

int main()
{
  // The value of the last expression.
  auto result = 0;
  // Number of errors seen during parsing.
  auto num_errors = 0;
  yy::parser parser(result, num_errors);

  // Scanner debug traces.
  if (getenv("YYSCAN"))
    {
      extern int yy_flex_debug;
      yy_flex_debug = 1;
    }

  // Parser debug traces.
  if (getenv("YYDEBUG"))
    parser.set_debug_level(1);

  // 0 if success, 1 otherwise.
  auto status = parser.parse() || num_errors;
  std::cerr << "Result: " << result << '\n';
  return status || num_errors;
}
