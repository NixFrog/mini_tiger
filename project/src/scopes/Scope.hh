#pragma once

#include <map>
#include <memory>
#include <boost/fusion/tuple.hpp>
#include <boost/fusion/algorithm/iteration/for_each.hpp>
#include <boost/fusion/include/for_each.hpp>

#include "../nodes/declarations/variables/Variable.hh"
#include "../common/utils/Exceptions.hh"

class ValueMapsKey_t;

namespace Kitten
{
    /* ! \struct AreSameType
    * Allows to check if T and D are of the same type
    * Use areSameType_v to check for type equality
    */
    template<class T, class D>
    struct AreSameType
    {
        static const bool areSameType_v = false;
    };

    /*
    * Specialization in case the types are the same
    */
    template<class T>
    struct AreSameType<T, T>
    {
        static const bool areSameType_v = true;
    };


    /* ! \class Scope
    * \brief Scope class containing maps to any number of types specified as a template
    * parameter.
    */
    template<typename ... TypesStored>
    class Scope
    {
    public:
        using ValueMapsKey_t = Variable;
        using ValueMapsTupleType_t = boost::fusion::tuple< std::map< ValueMapsKey_t, TypesStored > ... >;

        Scope();
        explicit Scope(std::shared_ptr< Scope< TypesStored ... >> parent);

        void addVariable(std::string variableName, const ValueMapsKey_t& variable);
        ValueMapsKey_t getVariable(std::string variableName) const;

        void setVariableType(std::string variableName, Types::Type type);

        template<typename T>
        void setValue(std::string variableName, T value);

        template<typename T>
        void setValue(const ValueMapsKey_t& variable, T value);

        template<typename T>
        T getValue(const ValueMapsKey_t& variable);

        std::shared_ptr<Scope<TypesStored ... >> getParentScope() const { return m_Parent; }

    private: // attributes
        std::map<std::string, ValueMapsKey_t> m_VariablesMap;
        ValueMapsTupleType_t m_ValueMaps;

        std::shared_ptr<Scope<TypesStored ... >> m_Parent;

    private: // functions
        bool variableIsDeclared(std::string variableName) const;

        /*
        * Grants access to the class's m_ValueMaps of type T.
        */
        template<typename T>
        std::map<ValueMapsKey_t, T>& getValueMapOfType();

    private: // fancy meta programming structs
        /*
        * Allows to check if the mapped type of the Nth map of a tuple of maps
        * is of the same type as T by checking areSameType_v
        */
        template<int N, typename T>
        struct MapIsOfSameType
            : AreSameType< T, typename boost::fusion::tuple_element<N, ValueMapsTupleType_t>::type::mapped_type >
        {};

        /* ! \struct MatchingField
        * \brief Structure to access a map with elements of type T among a Tuple of maps
        * Used if the type of the elements of the map at index N of a tuple is
        * not the same as T. Will check the next element of the Tuple.
        */
        template<int N, class T, class Tuple, bool Match = false>
        struct MatchingField
        {
            /* !
            * Gets the next map from a map tuple and checks if its element's type
            * is the same as T.
            * @param[tp] a tuple of type ValueMapsTupleType_t
            * @returns the next map from the given tuple
            */
            static std::map<ValueMapsKey_t, T>& get(Tuple& tp)
            {
                return MatchingField<N+1, T, Tuple, MapIsOfSameType<N+1, T>::areSameType_v>::get(tp);
            }
        };

        /* !
        * Specialization in case the map's elements' type and T match
        */
        template<int N, class T, class Tuple>
        struct MatchingField<N, T, Tuple, true>
        {
            /* !
            * Gets the next map from a map tuple and checks if its element's type
            * is the same as T.
            * @param[tp] a tuple of type ValueMapsTupleType_t
            * @returns the next map from the given tuple
            */
            static std::map<ValueMapsKey_t, T>& get(Tuple& tp)
            {
                return boost::fusion::get<N>(tp);
            }
        };
    };

    #include "Scope.tpp"
}
