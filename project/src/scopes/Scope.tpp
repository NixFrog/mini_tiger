#pragma once

template<typename ... T>
Scope<T ... >::Scope()
    : m_VariablesMap()
    , m_ValueMaps()
{
    m_Parent = NULL;
}

template<typename ... T>
Scope<T ... >::Scope(std::shared_ptr<Scope<T ... >> parent)
    : m_Parent(parent)
{}

template<typename ... T>
void Scope<T ...>::addVariable(std::string variableName, const ValueMapsKey_t& variable)
{
    std::pair<std::string, Variable> pairToInsert(variableName, variable);
    if(m_VariablesMap.insert(pairToInsert).second == false){
        throw MultipleDefinitionsException("Redefinition of variable '" + variableName + "'");
    }
}

//FIXME crashes due to static access before initialization
template<typename ... T>
typename Scope<T ... >::ValueMapsKey_t Scope<T ... >::getVariable(std::string variableName) const
{
    try{
        return m_VariablesMap.at(variableName);
    } catch(std::out_of_range e) {
        if(m_Parent != nullptr){
            try{
                return m_Parent->getVariable(variableName);
            } catch(std::out_of_range e) {
                throw(e);
            }
        }
        else{
            throw(e);
        }
    }
}

template<typename ... T>
void Scope<T ... >::setVariableType(std::string variableName, Types::Type type)
{
    auto i = 0;
    boost::fusion::for_each(m_ValueMaps,
        i = [=](auto i)
            {
                auto map = boost::fusion::get<i>(m_ValueMaps);
                map.at(variableName).setType(type);
                i++;
                return i;
            }
    );
}

template<typename ... T>
template<typename D>
void Scope< T ... >::setValue(std::string variableName, D value)
{
    auto variable = getVariable(variableName);
    setValue(variable, value);
}

template<typename ... T>
template<typename D>
void Scope< T ... >::setValue(const ValueMapsKey_t& variable, D value)
{
    if(variableIsDeclared(variable.getName())){
        try{
            getValueMapOfType< D >().at(variable) = value;
        } catch(std::out_of_range e){
            auto toInsert = std::pair<Variable, D>(variable, value);
            getValueMapOfType< D >().insert(toInsert);
        }
    }
    else{
        throw std::out_of_range("Scope::setValue() map::at");
    }
}

template<typename ... T>
template<typename D>
D Scope< T ... >::getValue(const ValueMapsKey_t& variable)
{
    auto mapToUse = getValueMapOfType< D >();

    try{
        return getValueMapOfType< D >().at(variable);
    } catch(std::out_of_range e) {
        if(m_Parent != nullptr){
            try{
                return m_Parent->template getValue< D >(variable);
            } catch(std::out_of_range e) {
                throw(e);
            }
        }
        else{
            throw(e);
        }
    }
}

template<typename ... T>
bool Scope< T ... >::variableIsDeclared(std::string variableName) const
{
    auto exists = false;
    if(m_VariablesMap.find(variableName) != m_VariablesMap.end()){
        exists = true;
    }
    else{
        if(m_Parent != nullptr)
        {
            exists = m_Parent->variableIsDeclared(variableName);
        }
    }

    return exists;
}

template<typename ... T>
template<typename D>
std::map<typename Scope< T ... >::ValueMapsKey_t, D>& Scope< T ... >::getValueMapOfType()
{
    return MatchingField<0, D, ValueMapsTupleType_t, MapIsOfSameType<0, D>::areSameType_v>::get(m_ValueMaps);
}
