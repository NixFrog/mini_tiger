namespace Kitten
{
    class Expression;
    class BinaryExpression;
    class Print;

    template<typename>
    class ConstantLeaf;
    class IfThenElse;
    class VoidExpression;

    template<typename> class ListNode;
    template<typename> class Declaration;
    class Variable;
    class LetIn;
    class Identifier;
    class Assignment;

    class PairOfExpressions;

    class While;

    class Parameter;
}
