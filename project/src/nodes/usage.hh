#include "fwd.hh"
#include <memory>

namespace Kitten
{
    using ExpPtr_t              = std::shared_ptr<Expression>;
    using VariableDeclaration_t = Declaration<Variable>;
    using VariableDeclarationPtr_t      = std::shared_ptr<VariableDeclaration_t>;
    using VariableDeclarationNode_t     = ListNode<VariableDeclarationPtr_t>;
    using VariableDeclarationNodePtr_t  = std::shared_ptr<VariableDeclarationNode_t>;
    using ParameterNode_t       = ListNode<std::shared_ptr<Parameter>>;
    using ParameterNodePtr_t    = std::shared_ptr<ParameterNode_t>;
}
