#pragma once

#include "../Expression.hh"
#include "Declarable.hh"

namespace Kitten
{
    template<typename T>
    class Declaration
        : public Expression
    {
    public:
        using DeclaredType_t = T;
        using DeclaredTypePtr_t = std::shared_ptr<DeclaredType_t>;

        Declaration(DeclaredTypePtr_t declared)
            : m_Declared(declared)
            , m_Expression()
        {}

        Declaration(DeclaredTypePtr_t declared, ExpPtr_t expression)
            : m_Declared(declared)
            , m_Expression(expression)
        {}

        void accept(Visitor& visitor) override       { visitor.visit(*this); }
        void accept(Visitor& visitor) const override { visitor.visit(*this); }

        DeclaredTypePtr_t getDeclared() const { return m_Declared; }
        ExpPtr_t getExpression() const        { return m_Expression; }

    private:
        DeclaredTypePtr_t m_Declared;
        ExpPtr_t m_Expression;
    };
}
