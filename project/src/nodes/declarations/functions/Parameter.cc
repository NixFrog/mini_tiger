#include "Parameter.hh"

namespace Kitten
{
    Parameter::Parameter(std::string name, Types::Type type)
        : m_Name(name)
        , m_Type(type)
    {}

    void Parameter::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void Parameter::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    std::string Parameter::getName() const
    {
        return m_Name;
    }

    Types::Type Parameter::getType() const
    {
        return m_Type;
    }
}
