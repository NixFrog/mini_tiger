#pragma once

#include "../../Expression.hh"
#include "../../../common/Types.hh"

namespace Kitten
{
    class Parameter
        : public Expression
    {
    public:
        Parameter(std::string name, Types::Type type);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        std::string getName() const;
        Types::Type getType() const;

    private:
        std::string m_Name;
        Types::Type m_Type;
    };
}
