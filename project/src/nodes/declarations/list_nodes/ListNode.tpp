#pragma once

template<typename T>
ListNode<T>::ListNode(T content)
    : m_Content(content)
    , m_NextNode()
{}

template<typename T>
ListNode<T>::ListNode(T content, NodePtr_t nextNode)
    : m_Content(content)
    , m_NextNode(nextNode)
{}

template<typename T>
void ListNode<T>::accept(Visitor& visitor)
{
    visitor.visit(*this);
}

template<typename T>
void ListNode<T>::accept(Visitor& visitor) const
{
    visitor.visit(*this);
}

template<typename T>
typename ListNode<T>::NodePtr_t ListNode<T>::getNextNode() const
{
    return m_NextNode;
}

template<typename T>
T ListNode<T>::getContent() const
{
    return m_Content;
}
