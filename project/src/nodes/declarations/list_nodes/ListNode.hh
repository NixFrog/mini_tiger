#pragma once

#include "../../Expression.hh"

namespace Kitten
{
    template<typename T>
    class ListNode
        : public Expression
    {
    public:
        using NodePtr_t = std::shared_ptr<ListNode<T>>;

        explicit ListNode(T content);
        ListNode(T content, NodePtr_t nextNode);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        T getContent() const;
        NodePtr_t getNextNode() const;

    private:
        T m_Content;
        NodePtr_t m_NextNode;
    };

    #include "ListNode.tpp"
}
