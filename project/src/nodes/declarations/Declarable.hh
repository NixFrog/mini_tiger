#pragma once

#include "../Expression.hh"
#include "../../common/Types.hh"

namespace Kitten
{
    class Declarable
        : public Expression
    {
    public:
        std::string getName() const {return m_Name;}
        Types::Type getType() const {return m_Type;}

    protected:
        Declarable(std::string name, Types::Type type): m_Name(name), m_Type(type){}

        std::string m_Name;
        Types::Type m_Type;
    };
}
