#include "LetIn.hh"

namespace Kitten
{
    LetIn::LetIn(DeclarationListPtr_t declarationListNode, ExpPtr_t expression)
        : m_DeclarationListNode(declarationListNode)
        , m_Expression(expression)
    {}

    void LetIn::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void LetIn::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    typename LetIn::DeclarationListPtr_t LetIn::getDeclarationListNode() const
    {
        return m_DeclarationListNode;
    }

    typename LetIn::ExpPtr_t LetIn::getExpression() const
    {
        return m_Expression;
    }
}
