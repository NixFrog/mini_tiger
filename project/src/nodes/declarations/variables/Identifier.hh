#pragma once

#include "../../Expression.hh"
#include <string>

namespace Kitten
{
    class Identifier
        : public Expression
    {
    public:
        Identifier(std::string name);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        std::string getName() const;

    private:
        std::string m_Name;
    };
}
