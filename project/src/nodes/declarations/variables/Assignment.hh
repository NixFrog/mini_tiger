#pragma once

#include "../../Expression.hh"

class Identifier;

namespace Kitten
{
    class Assignment
        : public Expression
    {
    public:
        Assignment(std::shared_ptr<Identifier> identifier, ExpPtr_t expression);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        std::shared_ptr<Identifier> getIdentifier() const;
        ExpPtr_t getExpression() const;

    private:
        std::shared_ptr<Identifier> m_Identifier;
        ExpPtr_t m_Expression;
    };
}
