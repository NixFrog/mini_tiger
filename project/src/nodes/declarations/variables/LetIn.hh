#pragma once

#include "../../Expression.hh"
#include "../Declaration.hh"

template<typename T>
class ListNode;

namespace Kitten
{
    class LetIn
        : public Expression
    {
    public:
        using DeclarationList_t =  ListNode<std::shared_ptr<Declaration<Variable>>>;
        using DeclarationListPtr_t =  std::shared_ptr<DeclarationList_t>;

        LetIn(DeclarationListPtr_t declarationListNode, ExpPtr_t expression);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        DeclarationListPtr_t getDeclarationListNode() const;
        ExpPtr_t getExpression() const;

    private:
        DeclarationListPtr_t m_DeclarationListNode;
        ExpPtr_t m_Expression;
    };
}
