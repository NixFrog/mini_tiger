#include "Identifier.hh"

namespace Kitten
{
    Identifier::Identifier(std::string name)
        : m_Name(name)
    {}

    void Identifier::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void Identifier::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    std::string Identifier::getName() const
    {
        return m_Name;
    }
}
