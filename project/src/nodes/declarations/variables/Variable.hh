#pragma once

#include "../Declarable.hh"

namespace Kitten
{
    class Variable
        : public Declarable
    {
    public:
        Variable(std::string name, Types::Type declaredType = Types::AUTO);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        void setType(Types::Type type);
    };

    inline bool operator<(const Variable& lhs, const Variable& rhs)
    {
        return ((lhs.getName() < rhs.getName()) || ((lhs.getName() == rhs.getName()) && (lhs.getType() < rhs.getType())));
    }
}
