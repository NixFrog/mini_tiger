#include "Variable.hh"

namespace Kitten
{
    Variable::Variable(std::string name, Types::Type declaredType)
        : Declarable(name, declaredType)
    {}

    void Variable::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void Variable::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    void Variable::setType(Types::Type type)
    {
        m_Type = type;
    }
}
