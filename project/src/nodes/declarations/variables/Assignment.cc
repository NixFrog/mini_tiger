#include "Assignment.hh"

namespace Kitten
{
    Assignment::Assignment(std::shared_ptr<Identifier> identifier, ExpPtr_t expression)
        : m_Identifier(identifier)
        , m_Expression(expression)
    {}

    void Assignment::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void Assignment::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    std::shared_ptr<Identifier> Assignment::getIdentifier() const
    {
        return m_Identifier;
    }

    typename Assignment::ExpPtr_t Assignment::getExpression() const
    {
        return m_Expression;
    }
}
