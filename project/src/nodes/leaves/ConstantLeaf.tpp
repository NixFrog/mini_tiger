#pragma once

template<typename T>
ConstantLeaf<T>::ConstantLeaf(const Value_t& value)
    : m_Value(value)
{}

template<typename T>
typename ConstantLeaf<T>::Value_t ConstantLeaf<T>::getValue() const
{
    return m_Value;
}

template<typename T>
void ConstantLeaf<T>::accept(Visitor& visitor)
{
    visitor.visit(*this);
}

template<typename T>
void ConstantLeaf<T>::accept(Visitor& visitor) const
{
    visitor.visit(*this);
}
