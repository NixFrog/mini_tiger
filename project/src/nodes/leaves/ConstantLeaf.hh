#pragma once

#include "../Expression.hh"

namespace Kitten
{
    template<typename T>
    class ConstantLeaf
        : public Expression
    {
    public:
        using Value_t = T;

        ConstantLeaf(const Value_t& value);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        Value_t getValue() const;

    private:
        Value_t m_Value;
    };

    #include "ConstantLeaf.tpp"
}
