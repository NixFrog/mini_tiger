#pragma once

#include "../Expression.hh"

namespace Kitten
{
    class VoidExpression
        : public Expression
    {
    public:
        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;
    private:
    };
}
