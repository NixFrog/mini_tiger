#include "VoidExpression.hh"

namespace Kitten
{
    void VoidExpression::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void VoidExpression::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }
}
