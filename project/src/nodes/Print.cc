#include "Print.hh"

namespace Kitten
{
    Print::Print(ExpPtr_t exp)
        : m_Exp(exp)
        , m_Type(Types::INVALID)
    {}

    void Print::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void Print::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }
}
