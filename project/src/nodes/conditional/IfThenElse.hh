#pragma once

#include "../Expression.hh"

namespace Kitten
{
    class IfThenElse
        : public Expression
    {
    public:
        IfThenElse(ExpPtr_t condition, ExpPtr_t consequence, ExpPtr_t otherwise);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        const ExpPtr_t getCondition() const  { return m_Condition; }
        const ExpPtr_t getConsequence() const{ return m_Consequence; }
        const ExpPtr_t getOtherwise() const  { return m_Otherwise; }

    private:
        ExpPtr_t m_Condition;
        ExpPtr_t m_Consequence;
        ExpPtr_t m_Otherwise;
    };
}
