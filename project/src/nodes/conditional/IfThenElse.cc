#include "IfThenElse.hh"

namespace Kitten
{
    IfThenElse::IfThenElse(ExpPtr_t condition, ExpPtr_t consequence, ExpPtr_t otherwise)
        : m_Condition(condition)
        , m_Consequence(consequence)
        , m_Otherwise(otherwise)
    {}

    void IfThenElse::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void IfThenElse::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }
}
