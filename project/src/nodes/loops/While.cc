#include "While.hh"

namespace Kitten
{
    While::While(ExpPtr_t condition, ExpPtr_t body)
        : m_Condition(condition)
        , m_Body(body)
    {}

    void While::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void While::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    typename While::ExpPtr_t While::getCondition() const
    {
        return m_Condition;
    }

    typename While::ExpPtr_t While::getBody() const
    {
        return m_Body;
    }
}
