#pragma once

#include "../Expression.hh"

namespace Kitten
{
    class While
        : public Expression
    {
    public:
        While(ExpPtr_t condition, ExpPtr_t body);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        ExpPtr_t getCondition() const;
        ExpPtr_t getBody() const;

    private:
        ExpPtr_t m_Condition;
        ExpPtr_t m_Body;
    };
}
