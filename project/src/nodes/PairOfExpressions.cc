#include "PairOfExpressions.hh"

namespace Kitten
{
    PairOfExpressions::PairOfExpressions(ExpPtr_t leftExpression, ExpPtr_t rightExpression)
        : m_LeftExpression(leftExpression)
        , m_RightExpression(rightExpression)
    {}

    void PairOfExpressions::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void PairOfExpressions::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }

    typename PairOfExpressions::ExpPtr_t PairOfExpressions::getLeftExpression() const
    {
        return m_LeftExpression;
    }

    typename PairOfExpressions::ExpPtr_t PairOfExpressions::getRightExpression() const
    {
        return m_RightExpression;
    }
}
