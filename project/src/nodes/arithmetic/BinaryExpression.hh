#pragma once

#include "../Expression.hh"

namespace Kitten
{
    class BinaryExpression
        : public Expression
    {
    public:
        enum OperatorType
        {
            Addition,
            Substraction,
            Multiplication,
            Division,
            Equality,
            Inequality,
            LesserOrEqualTo,
            LesserThan,
            GreaterOrEqualTo,
            GreaterThan,
            And,
            Or
        };

        BinaryExpression(OperatorType op, ExpPtr_t lhs, ExpPtr_t rhs);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        OperatorType getOperator() const {return m_Operator;}
        ExpPtr_t getLhs() const {return m_lhs;}
        ExpPtr_t getRhs() const {return m_rhs;}

    private:
        OperatorType m_Operator;
        ExpPtr_t m_lhs;
        ExpPtr_t m_rhs;
    };

    inline std::ostream& operator<<(std::ostream& os, const BinaryExpression::OperatorType& op)
    {
        switch(op)
        {
            case BinaryExpression::Addition:
                os << '+';
            break;
            case BinaryExpression::Substraction:
                os << '-';
            break;
            case BinaryExpression::Multiplication:
                os << '*';
            break;
            case BinaryExpression::Division:
                os << '/';
            break;
            case BinaryExpression::Equality:
                os << "==";
            break;
            case BinaryExpression::Inequality:
                os << "<>";
            break;
            case BinaryExpression::LesserOrEqualTo:
                os << "<=";
            break;
            case BinaryExpression::LesserThan:
                os << "<";
            break;
            case BinaryExpression::GreaterOrEqualTo:
                os << ">=";
            break;
            case BinaryExpression::GreaterThan:
                os << ">";
            break;
            case BinaryExpression::And:
                os << "&&";
            break;
            case BinaryExpression::Or:
                os << "||";
            break;
        }
        return os;
    }
}
