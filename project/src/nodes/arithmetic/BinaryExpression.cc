#include "BinaryExpression.hh"

namespace Kitten
{
    BinaryExpression::BinaryExpression(OperatorType op, ExpPtr_t lhs, ExpPtr_t rhs)
        : m_Operator(op)
    {
        m_lhs = lhs;
        m_rhs = rhs;
    }

    void BinaryExpression::accept(Visitor& visitor)
    {
        visitor.visit(*this);
    }

    void BinaryExpression::accept(Visitor& visitor) const
    {
        visitor.visit(*this);
    }
}
