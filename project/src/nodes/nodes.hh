#pragma once

#include "../nodes/leaves/ConstantLeaf.hh"
#include "../nodes/leaves/VoidExpression.hh"

#include "../nodes/arithmetic/BinaryExpression.hh"
#include "../nodes/Print.hh"

#include "../nodes/conditional/IfThenElse.hh"

#include "../nodes/declarations/Declaration.hh"
#include "../nodes/declarations/variables/Variable.hh"
#include "../nodes/declarations/variables/LetIn.hh"
#include "../nodes/declarations/variables/Identifier.hh"
#include "../nodes/declarations/variables/Assignment.hh"
#include "../nodes/declarations/list_nodes/ListNode.hh"

#include "../nodes/declarations/functions/Parameter.hh"

#include "../nodes/PairOfExpressions.hh"
#include "../nodes/loops/While.hh"
