#pragma once

#include "Expression.hh"

namespace Kitten
{
    class PairOfExpressions
        : public Expression
    {
    public:
        PairOfExpressions(ExpPtr_t leftExpression, ExpPtr_t rightExpression);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        ExpPtr_t getLeftExpression() const;
        ExpPtr_t getRightExpression() const;

    private:
        ExpPtr_t m_LeftExpression;
        ExpPtr_t m_RightExpression;
    };
}
