#pragma once

#include "Expression.hh"
#include "../common/Types.hh"

namespace Kitten
{
    class Print
        : public Expression
    {
    public:
        Print(ExpPtr_t exp);

        void accept(Visitor& visitor) override;
        void accept(Visitor& visitor) const override;

        ExpPtr_t getExp() const {return m_Exp; }
        Types::Type getType() const {return m_Type; }

    private:
        ExpPtr_t m_Exp;
        Types::Type m_Type;
    };
}
