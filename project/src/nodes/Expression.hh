#pragma once

#include <memory>

#include "../Ast.hh"
#include "../visitors/Visitor.hh"

namespace Kitten
{
    class Expression
        : public Ast
    {
    public:
        using ExpPtr_t = std::shared_ptr<Expression>;

        virtual ~Expression(){};
        virtual void accept(Visitor& visitor) = 0;
        virtual void accept(Visitor& visitor) const = 0;
    };
}
