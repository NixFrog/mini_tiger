#pragma once

#include <stdexcept>
#include <string>

namespace Kitten
{
    class WrongParameterException : public std::runtime_error {
    public:
        WrongParameterException(const std::string& message)
            : std::runtime_error(message)
        {}
    };

    class MultipleDefinitionsException : public std::runtime_error {
    public:
        MultipleDefinitionsException(const std::string& message)
            : std::runtime_error(message)
        {}
    };

}
