#pragma once

#include <string>
#include <map>

namespace Kitten
{
    namespace Types
    {
        enum Type
        {
            INVALID,
            VOID,
            STRING,
            INTEGER,
            AUTO
        };

        static std::map<std::string, Type> stringToTypeMap =
        {
            {"string", STRING },
            {"int",    INTEGER },
            {"auto",   AUTO }
        };

        static std::map<Type, std::string> typeToStringMap =
        {
            {INVALID, "invalid"},
            {VOID,    "void"},
            {STRING,  "string"},
            {INTEGER, "int"},
            {AUTO,    "auto"}
        };
    }
}
