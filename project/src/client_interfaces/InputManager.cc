#include "InputManager.hh"

namespace Kitten
{
    InputManager::InputManager(int argc, char const* argv[])
    {
        manageOptions(argv, argv + argc);
    }

    bool InputManager::optionIsSelected(const char** begin, const char** end, const std::string& option)
    {
        return std::find(begin, end, option) != end;
    }

    const char* InputManager::getOptionParameter(const char ** begin, const char ** end, const std::string & option)
    {
        auto itr = std::find(begin, end, option);
        if (itr != end && ++itr != end)
        {
            return *itr;
        }
        return 0;
    }

    void InputManager::manageOptions(const char** begin, const char** end)
    {
        if(optionIsSelected(begin, end, "-f")){
            manageFileOption(begin, end);
        }
        if(optionIsSelected(begin, end, "--no-pretty-print")){
            m_IsPrettyPrinting = false;
        }
        if(optionIsSelected(begin, end, "--debug")){
            m_IsPrompting = false;
            m_IsPrettyPrinting = false;
        }
    }

    void InputManager::manageFileOption(const char** begin, const char** end)
    {
        auto filename = getOptionParameter(begin, end, "-f");
        m_IsPrompting = false;
        m_InputFile = std::shared_ptr<std::FILE>(std::fopen(filename, "r"));
        if(!m_InputFile){
            throw WrongParameterException("Failed to open specified file.");
        }
    }

    std::shared_ptr<std::FILE> InputManager::getFileDescriptor()
    {
        return m_InputFile;
    }

    void InputManager::updateInput()
    {
        if(!m_IsPrompting){
            yyin = m_InputFile.get();
        }
        else{
            std::cout<<PROMPT;
        }
    }

    void InputManager::manageInput(std::shared_ptr<Expression> input)
    {
        prettyPrint(input);
        auto returnType = typeCheck(input);

        Kitten::ErrorManagerSingleton::getInstance().clearErrors();

        if(returnType != Types::INVALID){
            evaluate(input, returnType);
        }

        Kitten::ErrorManagerSingleton::getInstance().clearErrors();
    }

    void InputManager::prettyPrint(std::shared_ptr<Expression> input) const
    {
        PrettyPrinter prettyPrinter;
        if(m_IsPrettyPrinting == true){
            input->accept(prettyPrinter);
            std::cout<<"\nEval:\t";
        }
    }

    Types::Type InputManager::typeCheck(std::shared_ptr<Expression> input) const
    {
        auto isSafe = true;
        TypeChecker typeChecker;

        input->accept(typeChecker);
        auto typeErrorCount = Kitten::ErrorManagerSingleton::getInstance().getErrorCount();

        if(typeErrorCount != 0)
        {
            std::cerr<<"\n\nTypeChecking: "<< typeErrorCount<< " errors\n"<< Kitten::ErrorManagerSingleton::getInstance().getErrorString()<< std::endl;
            isSafe = false;
        }

        return typeChecker.getType();
    }


    void InputManager::evaluate(std::shared_ptr<Expression> input, Types::Type returnType) const
    {
        Evaluator evaluator;

        input->accept(evaluator);
        auto evalErrorCount = Kitten::ErrorManagerSingleton::getInstance().getErrorCount();

        if(evalErrorCount != 0)
        {
            std::cerr<<"\n\nEvaluation: "<< evalErrorCount<< " errors\n"<< Kitten::ErrorManagerSingleton::getInstance().getErrorString()<< std::endl;
        }
        else{
            std::cout<<"\nResult: ("<<Types::typeToStringMap.at(returnType)<<") ";
            if(returnType == Types::INTEGER){
                std::cout<< evaluator.getIntValue()<< std::endl;
            }
            else if(returnType == Types::STRING){
                std::cout<< evaluator.getStringValue() << std::endl;
            }
            else if(returnType == Types::VOID){
                std::cout<<std::endl;
            }
        }
        if(!m_IsPrompting){
            std::cout<<std::endl<<std::endl;
        }
    }

    bool InputManager::isPrompting()
    {
        return m_IsPrompting;
    }
}
