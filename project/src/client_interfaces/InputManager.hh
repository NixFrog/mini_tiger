#pragma once

#include <iostream>
#include <cstdlib>
#include <memory>
#include <algorithm>

#include "ErrorManager.hh"

#include "Singleton.hh"
#include "../common/utils/Exceptions.hh"
#include "../visitors/Evaluator.hh"
#include "../visitors/PrettyPrinter.hh"
#include "../visitors/TypeChecker.hh"

extern std::FILE* yyin;

namespace Kitten
{
    #define PROMPT ">>> "

    class InputManager
    {
    public:
        InputManager(){};
        InputManager(int argc, char const* argv[]);
        void manageOptions(const char** begin, const char** end);
        void updateInput();
        std::shared_ptr<std::FILE> getFileDescriptor();
        void manageInput(std::shared_ptr<Expression> input);
        bool isPrompting();

    private:
        bool optionIsSelected(const char** begin, const char** end, const std::string& option);
        const char* getOptionParameter(const char ** begin, const char ** end, const std::string & option);

        void manageFileOption(const char** begin, const char** end);
        void prettyPrint(std::shared_ptr<Expression> input) const;
        Types::Type typeCheck(std::shared_ptr<Expression> input) const;
        void evaluate(std::shared_ptr<Expression> input, Types::Type returnType) const;

        bool m_IsPrompting = true;
        bool m_IsPrettyPrinting = true;
        std::shared_ptr<std::FILE> m_InputFile;
    };

    using InputManagerSingleton = Singleton<InputManager>;
}
