#pragma once

#include "Singleton.hh"

#include "ErrorManager.hh"
#include "../common/Types.hh"

#include "../nodes/usage.hh"
#include "../nodes/nodes.hh"

namespace Kitten
{
    class NodeMaker
    {
    public:
        NodeMaker(){}

        template<typename T>
        std::shared_ptr<ConstantLeaf<T>> makeConstantLeaf(const T& value)
        {return std::make_shared<ConstantLeaf<T>>(ConstantLeaf<T>(value));}

        std::shared_ptr<VoidExpression>    makeVoidExpression();
        std::shared_ptr<BinaryExpression>  makeBinaryExpression(BinaryExpression::OperatorType op, ExpPtr_t lhs, ExpPtr_t rhs);
        std::shared_ptr<Print>              makePrint(ExpPtr_t exp);

        std::shared_ptr<PairOfExpressions> makePairOfExpressions(ExpPtr_t leftExpression, ExpPtr_t rightExpression);

        std::shared_ptr<IfThenElse>        makeIfThenElse(ExpPtr_t condition, ExpPtr_t consequence, ExpPtr_t otherwise);

        std::shared_ptr<Variable>   makeVariable(std::string name);
        std::shared_ptr<Variable>   makeVariable(std::string name, std::string type);
        std::shared_ptr<Assignment> makeAssignment(std::string name, ExpPtr_t value);
        VariableDeclarationPtr_t    makeVariableDeclaration(ExpPtr_t variable);
        VariableDeclarationPtr_t    makeVariableDeclaration(ExpPtr_t variable, ExpPtr_t expression);
        VariableDeclarationNodePtr_t makeDeclarationListNode(ExpPtr_t declaration);
        VariableDeclarationNodePtr_t makeDeclarationListNode(ExpPtr_t declaration, ExpPtr_t nextDeclarationListNode);
        std::shared_ptr<LetIn>      makeLetIn(ExpPtr_t declarationListNode, ExpPtr_t instructions);
        std::shared_ptr<Identifier> makeIdentifier(std::string name);

        std::shared_ptr<While>       makeWhile(ExpPtr_t condition, ExpPtr_t body);
        std::shared_ptr<LetIn>       makeFor(std::string variableName, ExpPtr_t value, ExpPtr_t limit, ExpPtr_t body);

        std::shared_ptr<Parameter> makeParameter(std::string name, std::string type);
        ParameterNodePtr_t         makeParameterListNode(ExpPtr_t parameter);
        ParameterNodePtr_t         makeParameterListNode(ExpPtr_t parameter, ExpPtr_t nextParameterListNode);

    private:
        Types::Type getDeclaredType(std::string type);
    };

    using NodeMakerSingleton = Singleton<NodeMaker>;
}
