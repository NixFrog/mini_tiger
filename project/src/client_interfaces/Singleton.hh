#pragma once

#include <memory>

namespace Kitten
{
    template<class T>
    class Singleton
    {
    public:
        static T& getInstance()
        {
            if(!m_Instance){
                m_Instance = std::unique_ptr<T>(new T());
            }
            return *m_Instance;
        }

    protected:
        Singleton();
        ~Singleton();

    private:
        Singleton(const Singleton&);
        Singleton& operator=(const Singleton&);
        static std::unique_ptr<T> m_Instance;
    };

    template<class T> std::unique_ptr<T> Singleton<T>::m_Instance=NULL;
}
