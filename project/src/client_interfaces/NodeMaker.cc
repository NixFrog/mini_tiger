#include "NodeMaker.hh"

namespace Kitten
{
    std::shared_ptr<BinaryExpression>
    NodeMaker::makeBinaryExpression(
        BinaryExpression::OperatorType op
        , ExpPtr_t lhs
        , ExpPtr_t rhs
    )
    {
        return std::make_shared<BinaryExpression>(BinaryExpression(op, lhs, rhs));
    }

    std::shared_ptr<Print> NodeMaker::makePrint(ExpPtr_t exp)
    {
        return std::make_shared<Print>(Print(exp));
    }

    std::shared_ptr<IfThenElse> NodeMaker::makeIfThenElse(
        ExpPtr_t condition, ExpPtr_t consequence, ExpPtr_t otherwise
    )
    {
        return std::make_shared<IfThenElse>(
            IfThenElse(condition, consequence, otherwise)
        );
    }

    std::shared_ptr<VoidExpression>
    NodeMaker::makeVoidExpression()
    {
        return std::make_shared<VoidExpression>(VoidExpression());
    }

    std::shared_ptr<Variable>
    NodeMaker::makeVariable(std::string name)
    {
        return std::make_shared<Variable>(Variable(name));
    }

    std::shared_ptr<Variable>
    NodeMaker::makeVariable(std::string name, std::string type)
    {
        auto declaredType = getDeclaredType(type);
        return std::make_shared<Variable>(Variable(name, declaredType));
    }

    typename Kitten::VariableDeclarationPtr_t
    NodeMaker::makeVariableDeclaration(ExpPtr_t variable)
    {
        return std::make_shared<VariableDeclaration_t>(
            VariableDeclaration_t(std::dynamic_pointer_cast<Variable>(variable))
        );
    }

    typename Kitten::VariableDeclarationPtr_t
    NodeMaker::makeVariableDeclaration(ExpPtr_t variable, ExpPtr_t expression)
    {
        return std::make_shared<VariableDeclaration_t>(
            VariableDeclaration_t(
                std::dynamic_pointer_cast<Variable>(variable)
                , expression
            )
        );
    }

    typename Kitten::VariableDeclarationNodePtr_t
    NodeMaker::makeDeclarationListNode(ExpPtr_t declaration)
    {
        return std::make_shared<VariableDeclarationNode_t>(
            std::dynamic_pointer_cast<VariableDeclaration_t>(declaration)
        );
    }

    typename Kitten::VariableDeclarationNodePtr_t
    NodeMaker::makeDeclarationListNode(ExpPtr_t declaration, ExpPtr_t nextDeclarationListNode)
    {
        return std::make_shared<VariableDeclarationNode_t>(
                std::dynamic_pointer_cast<VariableDeclaration_t>(declaration)
                , std::dynamic_pointer_cast< VariableDeclarationNode_t >(nextDeclarationListNode)
        );
    }

    std::shared_ptr<LetIn>
    NodeMaker::makeLetIn(ExpPtr_t declarationListNode, ExpPtr_t instructions)
    {
        return std::make_shared<LetIn>(
            LetIn(
                std::dynamic_pointer_cast<LetIn::DeclarationList_t>(declarationListNode)
                , instructions)
        );
    }

    std::shared_ptr<Identifier>
    NodeMaker::makeIdentifier(std::string name)
    {
        return std::make_shared<Identifier>(Identifier(name));
    }

    std::shared_ptr<PairOfExpressions>
    NodeMaker::makePairOfExpressions(ExpPtr_t leftExpression, ExpPtr_t rightExpression)
    {
        return std::make_shared<PairOfExpressions>(
            PairOfExpressions(leftExpression, rightExpression)
        );
    }

    std::shared_ptr<Assignment>
    NodeMaker::makeAssignment(std::string name, ExpPtr_t value)
    {
        return std::make_shared<Assignment>(
            Assignment( makeIdentifier(name), value)
        );
    }

    std::shared_ptr<While>
    NodeMaker::makeWhile(ExpPtr_t condition, ExpPtr_t body)
    {
        return std::make_shared<While>(While(condition, body));
    }

    std::shared_ptr<LetIn>
    NodeMaker::makeFor(std::string variableName, ExpPtr_t value, ExpPtr_t limit, ExpPtr_t body)
    {
        auto identifier  = makeIdentifier(variableName);
        auto variable    = makeVariable(variableName);
        auto declaration = makeVariableDeclaration(variable, value);
        auto declarationListNode = makeDeclarationListNode(declaration);

        auto comparison = makeBinaryExpression(BinaryExpression::LesserOrEqualTo, identifier, limit);
        auto one        = makeConstantLeaf<int>(1);
        auto increment  = makeBinaryExpression(BinaryExpression::Addition, identifier, one);
        auto assign     = makeAssignment(variableName, increment);
        auto whileBody  = makePairOfExpressions(body, assign);

        auto whileLoop  = makeWhile(comparison, whileBody);
        auto letIn      = makeLetIn(declarationListNode, whileLoop);

        return letIn;
    }

    std::shared_ptr<Parameter>
    NodeMaker::makeParameter(std::string name, std::string type)
    {
        auto declaredType = getDeclaredType(type);
        return std::make_shared<Parameter>(
            Parameter(name, declaredType)
        );
    }

    typename Kitten::ParameterNodePtr_t
    NodeMaker::makeParameterListNode(ExpPtr_t parameter)
    {
        return std::make_shared<ParameterNode_t>(
            std::dynamic_pointer_cast<Parameter>(parameter)
        );
    }

    typename Kitten::ParameterNodePtr_t
    NodeMaker::makeParameterListNode(ExpPtr_t parameter, ExpPtr_t nextParameterListNode)
    {
        return std::make_shared<ParameterNode_t>(
                std::dynamic_pointer_cast<Parameter>(parameter)
                , std::dynamic_pointer_cast< ParameterNode_t >(nextParameterListNode)
        );
    }

    Types::Type
    NodeMaker::getDeclaredType(std::string type)
    {
        auto declaredType = Types::INVALID;
        try {
            declaredType = Types::stringToTypeMap.at(type);
        } catch( std::out_of_range e) {
            ErrorManagerSingleton::getInstance().addError("Unknown type name '" + type +"'");
        }
        return declaredType;
    }
}
