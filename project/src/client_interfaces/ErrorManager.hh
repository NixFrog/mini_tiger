#pragma once

#include <string>
#include <memory>
#include "Singleton.hh"

namespace Kitten
{
    class ErrorManager
    {
    public:
        ErrorManager(): m_ErrorCount(0), m_ErrorString(""){};

        void addError(std::string errorString);
        void clearErrors();

        std::string getErrorString() const;
        size_t getErrorCount() const;

    private:
        size_t m_ErrorCount;
        std::string m_ErrorString;
    };

    using ErrorManagerSingleton = Singleton<ErrorManager>;
}
