#include "ErrorManager.hh"

namespace Kitten
{
    void ErrorManager::addError(std::string errorString)
    {
        m_ErrorString += "Error: " + errorString + "\n";
        m_ErrorCount++;
    }

    void ErrorManager::clearErrors()
    {
        m_ErrorString = "";
        m_ErrorCount = 0;
    }

    std::string ErrorManager::getErrorString() const
    {
        return m_ErrorString;
    }

    size_t ErrorManager::getErrorCount() const
    {
        return m_ErrorCount;
    }
}
