#pragma once

#include <iostream>
#include <memory>

#include "../nodes/usage.hh"
#include "BaseVisitor.hh"

namespace Kitten
{
    class Visitor
        : public BaseVisitor
    {
    public:
        virtual void visit(const ConstantLeaf<int>& ne) = 0;
        virtual void visit(const ConstantLeaf<std::string>& ne) = 0;

        virtual void visit(const BinaryExpression& be) = 0;
        virtual void visit(const Print& print) = 0;
        virtual void visit(const VoidExpression& ve) = 0;

        virtual void visit(const IfThenElse& ite) = 0;

        virtual void visit(const Variable& variable) = 0;
        virtual void visit(const VariableDeclaration_t& declaration) = 0;
        virtual void visit(const VariableDeclarationNode_t& declarationListNode) = 0;
        virtual void visit(const LetIn& letIn) = 0;
        virtual void visit(const Identifier& identifier) = 0;

        virtual void visit(const PairOfExpressions& pairOfExpressions) = 0;

        virtual void visit(const Assignment& assignment) = 0;
        virtual void visit(const While& whileLoop) = 0;

        virtual void visit(const Parameter& parameter) = 0;
        virtual void visit(const ParameterNode_t& parameterListNode) = 0;
    };
}
