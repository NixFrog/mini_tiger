#include "PrettyPrinter.hh"

namespace Kitten
{
    PrettyPrinter::PrettyPrinter(std::shared_ptr<Expression> exp)
        : PrettyPrinter()
    {
        exp->accept(*this);
    }

    void PrettyPrinter::visit(const ConstantLeaf<int>& ne)
    {
        std::cout<<ne.getValue();
    }

    void PrettyPrinter::visit(const ConstantLeaf<std::string>& ne)
    {
        std::cout<< "\""<< ne.getValue()<< "\"";
    }

    void PrettyPrinter::visit(const BinaryExpression& be)
    {
        std::cout<<"(";
        be.getLhs()->accept(*this);
        std::cout<<" "<<be.getOperator()<<" ";
        be.getRhs()->accept(*this);
        std::cout<<")";
    }

    void PrettyPrinter::visit(const Print& print)
    {
        std::cout<<"print ( ";
        print.getExp()->accept(*this);
        std::cout<<" )";
    }

    void PrettyPrinter::visit(const IfThenElse& ite)
    {
        m_NumberOfTabs++;
        std::cout<< "if (\n"<< getTabs();
        ite.getCondition()->accept(*this);
        m_NumberOfTabs--;

        std::cout<<"\n"<< getTabs()<< ") then (\n";
        m_NumberOfTabs++;
        std::cout<< getTabs();
        ite.getConsequence()->accept(*this);
        m_NumberOfTabs--;

        std::cout<<"\n"<< getTabs()<< ") else (\n";
        m_NumberOfTabs++;
        std::cout<< getTabs();
        ite.getOtherwise()->accept(*this);
        m_NumberOfTabs--;

        std::cout<<"\n"<< getTabs()<< ")";
    }

    void PrettyPrinter::visit(const VoidExpression&)
    {}

    void PrettyPrinter::visit(const Variable& variable)
    {
        auto name = variable.getName();
        auto type = Types::typeToStringMap.at(variable.getType());
        std::cout<< name<<" : "<< type;
    }

    void PrettyPrinter::visit(const VariableDeclaration_t& declaration)
    {
        std::cout<< "var ";
        declaration.getDeclared()->accept(*this);
        auto expression = declaration.getExpression();
        if(expression){
            std::cout<<" := ";
            expression->accept(*this);
        }
    }

    void PrettyPrinter::visit(const VariableDeclarationNode_t& declarationListNode)
    {
        auto nextDeclarationListNode = declarationListNode.getNextNode();
        declarationListNode.getContent()->accept(*this);

        if(nextDeclarationListNode != NULL){
            std::cout<<"\n"<<getTabs();
            nextDeclarationListNode->accept(*this);
        }
    }

    void PrettyPrinter::visit(const LetIn& letIn)
    {
        m_NumberOfTabs++;
        std::cout<<"let (\n"<<getTabs();
        letIn.getDeclarationListNode()->accept(*this);
        m_NumberOfTabs--;

        std::cout<<"\n"<< getTabs()<< ") in (\n";
        m_NumberOfTabs++;
        std::cout<< getTabs();
        letIn.getExpression()->accept(*this);
        m_NumberOfTabs--;

        std::cout<<"\n"<< getTabs()<< ") end";
    }

    void PrettyPrinter::visit(const Identifier& identifier)
    {
        std::cout<<identifier.getName();
    }

    void PrettyPrinter::visit(const PairOfExpressions& pairOfExpressions)
    {
        pairOfExpressions.getLeftExpression()->accept(*this);
        std::cout<<", \n"<<getTabs();
        pairOfExpressions.getRightExpression()->accept(*this);
    }

    void PrettyPrinter::visit(const Assignment& assignment)
    {
        assignment.getIdentifier()->accept(*this);
        std::cout<<" := ";
        assignment.getExpression()->accept(*this);
    }

    void PrettyPrinter::visit(const While& whileLoop)
    {
        std::cout<<"while ( ";
        whileLoop.getCondition()->accept(*this);

        std::cout<<" )\n"<< getTabs()<<"do ( "<<std::endl;
        m_NumberOfTabs++;
        std::cout<< getTabs();
        whileLoop.getBody()->accept(*this);
        m_NumberOfTabs--;
        std::cout<<"\n"<< getTabs()<< ")";
    }

    void PrettyPrinter::visit(const Parameter& parameter)
    {
        auto name =  parameter.getName();
        auto typeString = Types::typeToStringMap.at(parameter.getType());
        std::cout<< name << " : "<< typeString;
    }

    void PrettyPrinter::visit(const ParameterNode_t& parameterListNode)
    {
        auto nextDeclarationListNode = parameterListNode.getNextNode();
        parameterListNode.getContent()->accept(*this);

        if(nextDeclarationListNode != NULL){
            std::cout<<"\n"<<getTabs();
            nextDeclarationListNode->accept(*this);
        }
    }

    std::string PrettyPrinter::getTabs() const
    {
        std::string str = "";
        for(size_t i = 0; i<m_NumberOfTabs; ++i){
            str += ". . ";
        }

        return str;
    }
}
