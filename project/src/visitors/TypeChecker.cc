#include "TypeChecker.hh"

namespace Kitten
{
    TypeChecker::TypeChecker(std::shared_ptr<Expression> exp)
    {
        exp->accept(*this);
    }

    void TypeChecker::visit(const ConstantLeaf<int>&)
    {
        m_Type = Types::INTEGER;
    }

    void TypeChecker::visit(const ConstantLeaf<std::string>&)
    {
        m_Type = Types::STRING;
    }

    void TypeChecker::visit(const BinaryExpression& be)
    {
        be.getLhs()->accept(*this);
        auto lhsType = m_Type;

        be.getRhs()->accept(*this);
        auto rhsType = m_Type;

        if(lhsType != rhsType){
            ErrorManagerSingleton::getInstance().addError(
                "Unmatching operand types for binary expression: '"
                + Types::typeToStringMap.at(lhsType)
                + "' and '"
                + Types::typeToStringMap.at(rhsType)
                + "'"
            );

            m_Type = Types::INVALID;
        }
        else if(lhsType == Types::STRING){
            auto binaryOperator = be.getOperator();
             auto forbiddenList = {
                BinaryExpression::Or,
                BinaryExpression::And,
                BinaryExpression::Division,
                BinaryExpression::Multiplication,
                BinaryExpression::Substraction
            };
            for(auto i: forbiddenList){
                if(lhsType == Types::STRING && i == binaryOperator){
                    std::string a;
                    ErrorManagerSingleton::getInstance().addError(
                        "Invalid operands to binary expression: '"
                        + Types::typeToStringMap.at(lhsType)
                        + "' and '"
                        + Types::typeToStringMap.at(rhsType)
                        + "'"
                    );
                    m_Type = Types::INVALID;
                }
            }
        }
    }

    void TypeChecker::visit(const Print& print)
    {
        print.getExp()->accept(*this);
        m_Type = Types::VOID;
    }

    void TypeChecker::visit(const IfThenElse& ite)
    {
        ite.getCondition()->accept(*this);
        auto conditionType = m_Type;
        if(conditionType != Types::INTEGER){
            ErrorManagerSingleton::getInstance().addError(
                "Wrong conditional type: expected INTEGER, found '"
                + Types::typeToStringMap.at(conditionType)
                + "'"
            );
        }

        ite.getConsequence()->accept(*this);
        auto consequenceType = m_Type;

        ite.getOtherwise()->accept(*this);
        auto otherwiseType = m_Type;

        if(consequenceType != otherwiseType){
            ErrorManagerSingleton::getInstance().addError(
                "Cannot deduce return type due to unmatchig expression types: '"
                + Types::typeToStringMap.at(consequenceType)
                + "' and '"
                + Types::typeToStringMap.at(otherwiseType)
                + "'"
            );
            m_Type = Types::INVALID;
        }
        else if(conditionType != Types::INTEGER){
            m_Type = Types::INVALID;
        }
    }

    void TypeChecker::visit(const VoidExpression&)
    {
        m_Type = Types::VOID;
    }

    void TypeChecker::visit(const Variable& variable)
    {
        m_Type = variable.getType();
    }

    void TypeChecker::visit(const VariableDeclaration_t& declaration)
    {
        auto variable     = declaration.getDeclared();
        auto variableType = variable->getType();
        auto expression   = declaration.getExpression();

        if(expression){
            expression->accept(*this);
            auto declarationType = m_Type;

            if(variableType == Types::AUTO){
                variable->setType(declarationType);
            }
            else if(variableType != declarationType){
                ErrorManagerSingleton::getInstance().addError(
                    "Cannot initialize a variable of type '"
                    + Types::typeToStringMap.at(variableType)
                    + "' with an lvalue of type '"
                    + Types::typeToStringMap.at(declarationType)
                    + "'"
                );
                variable->setType(Types::INVALID);
                m_Type = Types::INVALID;
            }
        }
        else{
            if(variableType == Types::AUTO){
                ErrorManagerSingleton::getInstance().addError(
                    "Declaration of variable '"
                    +variable->getName()
                    +"' With type 'auto' requires an initialization."
                );
            }
        }

        m_Scope->addVariable(variable->getName(), *variable);
    }

    void TypeChecker::visit(const VariableDeclarationNode_t& declarationListNode)
    {
        auto nextDeclarationListNode = declarationListNode.getNextNode();
        declarationListNode.getContent()->accept(*this);

        if(nextDeclarationListNode != NULL){
            nextDeclarationListNode->accept(*this);
        }
    }

    void TypeChecker::visit(const LetIn& letIn)
    {
        enterNewScope();
        letIn.getDeclarationListNode()->accept(*this);
        letIn.getExpression()->accept(*this);
        returnToParentScope();
    }

    void TypeChecker::visit(const Identifier& identifier)
    {
        auto variableName = identifier.getName();
        Types::Type variableType;

        try{
            variableType = m_Scope->getVariable(variableName).getType();
        } catch(std::out_of_range e) {
            ErrorManagerSingleton::getInstance().addError(
                "Use of undeclared identifier: '"
                + variableName
                + "'"
            );
        }

        m_Type = variableType;
    }

    void TypeChecker::visit(const PairOfExpressions& pairOfExpressions)
    {
        pairOfExpressions.getLeftExpression()->accept(*this);
        auto leftType = m_Type;

        pairOfExpressions.getRightExpression()->accept(*this);
        auto rightType = m_Type;

        if(leftType != Types::INVALID && rightType != Types::INVALID){
            m_Type = Types::VOID;
        }
        else{
            m_Type = Types::INVALID;
        }
    }

    void TypeChecker::visit(const Assignment& assignment)
    {
        auto identifier = assignment.getIdentifier();
        identifier->accept(*this);
        auto variableType = m_Type;
        assignment.getExpression()->accept(*this);
        auto expressionType = m_Type;

        // Let's think of the future...
        if(variableType == Types::AUTO){
            auto variableName = identifier->getName();
            m_Scope->getVariable(variableName).setType(expressionType);
        }

        if(variableType != expressionType){
            ErrorManagerSingleton::getInstance().addError(
                "Assigning to '"
                + Types::typeToStringMap.at(variableType)
                + "' from incompatible type'"
                + Types::typeToStringMap.at(expressionType)
                + "'"
            );
            m_Type = Types::INVALID;
        }
    }

    void TypeChecker::visit(const While& whileLoop)
    {
        whileLoop.getCondition()->accept(*this);
        auto conditionType = m_Type;

        whileLoop.getBody()->accept(*this);

        if(conditionType != Types::INTEGER){
            ErrorManagerSingleton::getInstance().addError(
                "While condition does not have INTEGER type. Has '"
                + Types::typeToStringMap.at(conditionType)
                + "' type instead."
            );
            m_Type = Types::INVALID;
        }
        else{
            m_Type = Types::VOID;
        }
    }

    void TypeChecker::visit(const Parameter& parameter)
    {
        m_Type = parameter.getType();
    }

    void TypeChecker::visit(const ParameterNode_t& parameterListNode)
    {
        auto nextParameterListNode = parameterListNode.getNextNode();
        parameterListNode.getContent()->accept(*this);

        if(nextParameterListNode != NULL){
            nextParameterListNode->accept(*this);
        }
    }

    void TypeChecker::enterNewScope()
    {
        m_Scope = std::make_shared<Scope<>>(Scope<>(m_Scope));
    }

    void TypeChecker::returnToParentScope()
    {
        m_Scope = m_Scope->getParentScope();
    }
}
