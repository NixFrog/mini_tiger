#pragma once

#include "Visitor.hh"

#include "../nodes/nodes.hh"
#include "../scopes/Scope.hh"
#include "../client_interfaces/ErrorManager.hh"

#include <stdexcept>

namespace Kitten
{
    class Evaluator
        : public Visitor
    {
    public:
        using Scope_t = Scope<int, std::string>;

        Evaluator();
        explicit Evaluator(std::shared_ptr<Expression> exp);

        void visit(const ConstantLeaf<int>& ne) override;
        void visit(const ConstantLeaf<std::string>& ne) override;
        void visit(const BinaryExpression& be) override;
        void visit(const Print& print) override;

        void visit(const IfThenElse& ite) override;
        void visit(const VoidExpression& ve) override;

        void visit(const Variable& variable) override;
        void visit(const VariableDeclaration_t& declaration) override;
        void visit(const VariableDeclarationNode_t& declarationListNode) override;
        void visit(const LetIn& letIn) override;
        void visit(const Identifier& identifier) override;

        void visit(const PairOfExpressions& pairOfExpressions) override;

        void visit(const Assignment& assignment) override;
        void visit(const While& whileLoop) override;

        void visit(const Parameter& parameter) override;
        void visit(const ParameterNode_t& parameterListNode) override;

        int getIntValue() const {return m_IntValue;}
        std::string getStringValue() const {return m_StringValue;}

    private:
        void enterNewScope();
        void returnToParentScope();

        std::string m_StringValue;
        int m_IntValue;
        std::shared_ptr<Scope_t> m_Scope;
    };
}
