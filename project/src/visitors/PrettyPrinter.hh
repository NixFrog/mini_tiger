#pragma once

#include "Visitor.hh"

#include "../nodes/nodes.hh"
#include "../scopes/Scope.hh"

namespace Kitten
{
    class PrettyPrinter
        : public Visitor
    {
    public:
        PrettyPrinter(): m_NumberOfTabs(0){}
        explicit PrettyPrinter(std::shared_ptr<Expression> exp);

        void visit(const ConstantLeaf<int>& ne) override;
        void visit(const ConstantLeaf<std::string>& ne) override;

        void visit(const BinaryExpression& be) override;
        void visit(const Print& print) override;

        void visit(const IfThenElse& ite) override;
        void visit(const VoidExpression& ve) override;

        void visit(const Variable& variable) override;
        void visit(const VariableDeclaration_t& declaration) override;
        void visit(const VariableDeclarationNode_t& declarationListNode) override;

        void visit(const LetIn& letIn) override;
        void visit(const Identifier& identifier) override;

        void visit(const PairOfExpressions& pairOfExpressions) override;

        void visit(const Assignment& assignment) override;
        void visit(const While& whileLoop) override;

        void visit(const Parameter& parameter) override;
        void visit(const ParameterNode_t& parameterListNode) override;

    private:
        std::string getTabs() const;

        size_t m_NumberOfTabs;
    };
}
