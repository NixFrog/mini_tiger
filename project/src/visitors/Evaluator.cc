#include "Evaluator.hh"

namespace Kitten
{
    Evaluator::Evaluator()
        : m_Scope()
    {}

    Evaluator::Evaluator(std::shared_ptr<Expression> exp)
        : Evaluator()
    {
        exp->accept(*this);
    }

    void Evaluator::visit(const ConstantLeaf<int>& ne)
    {
        m_IntValue = ne.getValue();
    }

    void Evaluator::visit(const ConstantLeaf<std::string>& ne)
    {
        m_StringValue = ne.getValue();
    }

    void Evaluator::visit(const BinaryExpression& be)
    {
        be.getLhs()->accept(*this);
        auto lValue = m_IntValue;
        auto lStringValue = m_StringValue;
        be.getRhs()->accept(*this);
        auto rValue = m_IntValue;
        auto rStringValue = m_StringValue;

        switch(be.getOperator())
        {
            case BinaryExpression::Addition:
                m_IntValue = lValue + rValue;
                m_StringValue = lStringValue + rStringValue;
            break;
            case BinaryExpression::Substraction:
                m_IntValue = lValue - rValue;
            break;
            case BinaryExpression::Multiplication:
                m_IntValue = lValue * rValue;
            break;
            case BinaryExpression::Division:
                if(rValue == 0){
                    ErrorManagerSingleton::getInstance().addError(
                        "Integer division with denominator = zero"
                    );
                    break;
                }
                m_IntValue = lValue / rValue;
            break;
            case BinaryExpression::Equality:
                m_IntValue = (lValue == rValue) ? true : false;
                m_StringValue = (lStringValue == rStringValue) ? true : false;
            break;
            case BinaryExpression::Inequality:
                m_IntValue = (lValue != rValue) ? true : false;
                m_StringValue = (lStringValue != rStringValue) ? true : false;
            break;
            case BinaryExpression::LesserOrEqualTo:
                m_IntValue = (lValue <= rValue) ? true : false;
                m_StringValue = (lStringValue <= rStringValue) ? true : false;
            break;
            case BinaryExpression::LesserThan:
                m_IntValue = (lValue < rValue) ? true : false;
                m_StringValue = (lStringValue < rStringValue) ? true : false;
            break;
            case BinaryExpression::GreaterOrEqualTo:
                m_IntValue = (lValue >= rValue) ? true : false;
                m_StringValue = (lStringValue >= rStringValue) ? true : false;
            break;
            case BinaryExpression::GreaterThan:
                m_IntValue = (lValue > rValue) ? true : false;
                m_StringValue = (lStringValue > rStringValue) ? true : false;
            break;
            case BinaryExpression::And:
                m_IntValue = (lValue && rValue) ? true : false;
            break;
            case BinaryExpression::Or:
                m_IntValue = (lValue || rValue) ? true : false;
            break;
        }
    }

    void Evaluator::visit(const Print& print)
    {
        print.getExp()->accept(*this);
        std::cout<< m_IntValue<< " " << m_StringValue;
    }

    void Evaluator::visit(const IfThenElse& ite)
    {
        ite.getCondition()->accept(*this);
        auto condition = m_IntValue;
        if(condition){
            ite.getConsequence()->accept(*this);
        }
        else{
            ite.getOtherwise()->accept(*this);
        }
    }

    void Evaluator::visit(const VoidExpression&)
    {}

    //nb: this should NOT happen. It's here just in case.
    void Evaluator::visit(const Variable&)
    {
        ErrorManagerSingleton::getInstance().addError(
            "Compiler error: variables should not be evaluated. This compiler is garbage."
        );
    }

    void Evaluator::visit(const VariableDeclaration_t& declaration)
    {
        auto variable     = declaration.getDeclared();
        auto variableName = variable->getName();
        auto expression   = declaration.getExpression();

        if(expression){
            expression->accept(*this);
        }
        else{
            m_IntValue = 0;
            m_StringValue = "";
        }

        try{
            m_Scope->addVariable(variableName, *variable);
        } catch(std::runtime_error re){
            ErrorManagerSingleton::getInstance().addError(re.what());
        }
        try{
            auto variableType = variable->getType();
            if(variableType == Types::INTEGER){
                auto variableValue = m_IntValue;
                m_Scope->setValue<int>(*variable, variableValue);
            }
            else if(variableType == Types::STRING){
                auto variableValue = m_StringValue;
                m_Scope->setValue<std::string>(*variable, variableValue);
            }
            else{
                ErrorManagerSingleton::getInstance().addError(
                    "Setting variable of unknown type '"
                    + Types::typeToStringMap.at(variableType)
                    + "'"
                );
            }
        } catch(std::out_of_range e){
            ErrorManagerSingleton::getInstance().addError(
                "Compiler error: setting declaration value."
            );
        }
    }

    void Evaluator::visit(const VariableDeclarationNode_t& declarationListNode)
    {
        auto nextDeclarationListNode = declarationListNode.getNextNode();
        declarationListNode.getContent()->accept(*this);

        if(nextDeclarationListNode != NULL){
            nextDeclarationListNode->accept(*this);
        }
    }

    void Evaluator::visit(const LetIn& letIn)
    {
        enterNewScope();
        letIn.getDeclarationListNode()->accept(*this);
        letIn.getExpression()->accept(*this);
        returnToParentScope();
    }

    void Evaluator::visit(const Identifier& identifier)
    {
        auto name = identifier.getName();
        try{
            auto variable = m_Scope->getVariable(identifier.getName());
            auto variableType = variable.getType();

            if(variableType == Types::INTEGER){
                m_IntValue = m_Scope->getValue<int>(variable);
            }
            else if(variableType == Types::STRING){
                m_StringValue = m_Scope->getValue<std::string>(variable);
            }
            else{
                ErrorManagerSingleton::getInstance().addError(
                    "Use of variable of unsupported type '"
                    + Types::typeToStringMap.at(variableType)
                    + "' in an operation."
                );
            }
        } catch(std::out_of_range e){
            ErrorManagerSingleton::getInstance().addError(
                "Use of undeclared identifier '"
                + name
                + "'"
            );
        }
    }

    void Evaluator::visit(const PairOfExpressions& pairOfExpressions)
    {
        pairOfExpressions.getLeftExpression()->accept(*this);
        pairOfExpressions.getRightExpression()->accept(*this);
    }

    void Evaluator::visit(const Assignment& assignment)
    {
        auto name = assignment.getIdentifier()->getName();
        assignment.getExpression()->accept(*this);

        try{
            auto variable = m_Scope->getVariable(name);
            auto variableType = variable.getType();

            if(variableType == Types::INTEGER){
                m_Scope->setValue<int>(name, m_IntValue);
            }
            else if(variableType == Types::STRING){
                m_Scope->setValue<std::string>(name, m_StringValue);
            }

        } catch(std::out_of_range e){
            ErrorManagerSingleton::getInstance().addError(
                "Use of undeclared identifier "
                + name
                + "'"
            );
        }
    }

    void Evaluator::visit(const While& whileLoop)
    {
        whileLoop.getCondition()->accept(*this);
        auto tmp = m_IntValue;
        while(tmp){
            whileLoop.getBody()->accept(*this);
            whileLoop.getCondition()->accept(*this);
            tmp = m_IntValue;
        }
    }

    void Evaluator::visit(const Parameter&)
    {
    }

    void Evaluator::visit(const ParameterNode_t&)
    {
    }

    void Evaluator::enterNewScope()
    {
        m_Scope = std::make_shared<Scope_t>(Scope_t(m_Scope));
    }

    void Evaluator::returnToParentScope()
    {
        m_Scope = m_Scope->getParentScope();
    }
}
