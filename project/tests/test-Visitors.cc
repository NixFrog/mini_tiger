#include "../src/client_interfaces/NodeMaker.hh"
#include "../src/visitors/TypeChecker.hh"
#include "../src/visitors/Evaluator.hh"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace Kitten;

BOOST_AUTO_TEST_SUITE(EvaluatorSuite)

BOOST_AUTO_TEST_CASE(EvaluatorTestsArithmetic)
{
    Evaluator evaluator;
    auto test_1 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(1);//1;
    auto test_2 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(2);//2;
    auto test_4 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(4);//4;

    auto test_1_plus_1 = NodeMakerSingleton::getInstance().makeBinaryExpression(BinaryExpression::Addition,test_1,test_1);//1+1;
    auto test_1_minus_1 = NodeMakerSingleton::getInstance().makeBinaryExpression(BinaryExpression::Substraction,test_1,test_1);//1-1;
    auto test_1_fois_2 = NodeMakerSingleton::getInstance().makeBinaryExpression(BinaryExpression::Multiplication,test_1,test_2);//1*2;
    auto test_4_divi_2 = NodeMakerSingleton::getInstance().makeBinaryExpression(BinaryExpression::Division,test_4,test_2);//4/2;

    BOOST_CHECK(test_1->getValue() == 1);

    test_1_plus_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue() == 2);

    test_1_minus_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue() == 0);

    test_1_fois_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue() == 2);

    test_4_divi_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue() == 2);
}

BOOST_AUTO_TEST_CASE(EvaluatorTestsLogicalBinExp)
{
    Evaluator evaluator;

    auto test_0               = NodeMakerSingleton::getInstance().makeConstantLeaf< int>(0); //0;
    auto test_1               = NodeMakerSingleton::getInstance().makeConstantLeaf< int>(1); //1;
    auto test_2               = NodeMakerSingleton::getInstance().makeConstantLeaf< int>(2); //2;
    auto test_1_equal_1       = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Equality,test_1,test_1); //1==1;
    auto test_2_equal_1       = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Equality,test_2,test_1); //2==1;
    auto test_1_equal_2       = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Equality,test_1,test_2); //1==2;
    auto test_1_less_2        = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::LesserThan,test_1,test_2); //1<2;
    auto test_1_less_1        = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::LesserThan,test_1,test_1); //1<1;
    auto test_1_less_0        = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::LesserThan,test_1,test_0); //1<0;
    auto test_1_larger_2      = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::GreaterThan,test_1,test_2); //1>2;
    auto test_1_larger_1      = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::GreaterThan,test_1,test_1); //1>1;
    auto test_1_larger_0      = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::GreaterThan,test_1,test_0); //1>0;
    auto test_1_lessequal_2   = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::LesserOrEqualTo,test_1,test_2); //1<=2;
    auto test_1_lessequal_1   = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::LesserOrEqualTo,test_1,test_1); //1<=1;
    auto test_1_lessequal_0   = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::LesserOrEqualTo,test_1,test_0); //1<=0;
    auto test_1_largerequal_2 = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::GreaterOrEqualTo,test_1,test_2); //1>=2;
    auto test_1_largerequal_1 = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::GreaterOrEqualTo,test_1,test_1); //1>=1;
    auto test_1_largerequal_0 = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::GreaterOrEqualTo,test_1,test_0); //1>=0;
    auto test_1_and_1         = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::And,test_1,test_1); //1&&1;
    auto test_1_and_0         = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::And,test_1,test_0); //1&&0;
    auto test_0_and_0         = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::And,test_0,test_0); //0&&0;
    auto test_1_or_1          = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Or,test_1,test_1); //1||1;
    auto test_1_or_0          = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Or,test_1,test_0); //1||0;
    auto test_0_or_0          = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Or,test_0,test_0); //0||0;

    test_1_equal_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_2_equal_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
    test_1_equal_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);

    test_1_less_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_1_less_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
    test_1_less_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);

    test_1_larger_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
    test_1_larger_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
    test_1_larger_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);

    test_1_lessequal_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_1_lessequal_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_1_lessequal_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);

    test_1_largerequal_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
    test_1_largerequal_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_1_largerequal_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);

    test_1_and_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_1_and_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
    test_0_and_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);

    test_1_or_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_1_or_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
    test_0_or_0->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==0);
}

BOOST_AUTO_TEST_CASE(EvaluatorTestsLetIn)
{
    Evaluator evaluator;
    auto test_1 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(1); //1;
    auto test_a = NodeMakerSingleton::getInstance().makeConstantLeaf<std::string>("a"); //a;

    auto test_variable1        = NodeMakerSingleton::getInstance().makeVariable("a"); //a
    auto test_declaration1     = NodeMakerSingleton::getInstance().makeVariableDeclaration(test_variable1, test_1); //var a:=1
    auto test_declarationList1 = NodeMakerSingleton::getInstance().makeDeclarationListNode(test_declaration1);

    auto test_declMgt_1 = NodeMakerSingleton::getInstance().makeLetIn(test_declarationList1, test_1); //let var a:=1 in 1 end;
    auto test_declMgt_2 = NodeMakerSingleton::getInstance().makeLetIn(test_declarationList1, test_a); //let var a:=1 in a end;

    test_declMgt_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);

    test_declMgt_2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);
}

BOOST_AUTO_TEST_CASE(EvaluatorTestsIfThenElse)
{
    Evaluator evaluator;

    auto test_0 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(0); //0;
    auto test_1 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(1); //1;
    auto test_2 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(2); //2;
    auto test_3 = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(3); //3;

    auto test_0_equal_0 = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Equality,test_0,test_0);//0==0;
    auto test_1_equal_1 = NodeMakerSingleton::getInstance().makeBinaryExpression( BinaryExpression::Equality,test_1,test_1); //1==1;

    auto test_if_else_1 = NodeMakerSingleton::getInstance().makeIfThenElse(test_1_equal_1, test_2, test_3);
    test_if_else_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==2);

    // 0==0 && 1 == 1
    auto test_condition1 = NodeMakerSingleton::getInstance().makeBinaryExpression(BinaryExpression::And,test_0_equal_0,test_1_equal_1);
    //if 0==0 && 1 == 1 then 0 else 1
     auto test_if_else1 = NodeMakerSingleton::getInstance().makeIfThenElse(test_condition1, test_0, test_1);
    //if if 0==0 && 1 == 1 then 0 else 1 then 1+2 else 2+3;
     auto test_if_else2 = NodeMakerSingleton::getInstance().makeIfThenElse(test_if_else1, test_2, test_3);

    test_if_else2->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==3);
}

BOOST_AUTO_TEST_CASE(EvaluatorTestsPrint)
{
    Kitten::Evaluator evaluator;

    auto test_1 = Kitten::NodeMakerSingleton::getInstance().makeConstantLeaf<int>(1);//1;
    auto test_a = Kitten::NodeMakerSingleton::getInstance().makeConstantLeaf<std::string>("a");//a;
    auto test_print_1 = Kitten::NodeMakerSingleton::getInstance().makePrint(test_1);//print(1);
    auto test_print_a = Kitten::NodeMakerSingleton::getInstance().makePrint(test_a);//print(a);

    test_print_1->accept(evaluator);
    BOOST_CHECK(evaluator.getIntValue()==1);

    test_print_a->accept(evaluator);
    BOOST_CHECK(evaluator.getStringValue()=="a");
}

BOOST_AUTO_TEST_SUITE_END()
