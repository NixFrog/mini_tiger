#include "../src/client_interfaces/NodeMaker.hh"

#define BOOST_TEST_DYN_LINK
#include <boost/test/unit_test.hpp>

using namespace Kitten;

BOOST_AUTO_TEST_SUITE(NodeMakerSuite)

BOOST_AUTO_TEST_CASE(NodeMakerCalls)
{
    auto leafInt    = NodeMakerSingleton::getInstance().makeConstantLeaf<int>(1);
    auto leafString = NodeMakerSingleton::getInstance().makeConstantLeaf<std::string>("Hello");
    BOOST_CHECK(leafInt->getValue() == 1);
    BOOST_CHECK(leafString->getValue() == "Hello");

    auto binaryExpression = NodeMakerSingleton::getInstance().makeBinaryExpression(BinaryExpression::Addition, leafInt, leafString);
    BOOST_CHECK(binaryExpression->getLhs() == leafInt);
    BOOST_CHECK(binaryExpression->getRhs() == leafString);

    auto ifThenElse = NodeMakerSingleton::getInstance().makeIfThenElse(leafInt, leafString, binaryExpression);
    BOOST_CHECK(ifThenElse->getCondition() == leafInt);
    BOOST_CHECK(ifThenElse->getConsequence() == leafString);
    BOOST_CHECK(ifThenElse->getOtherwise() == binaryExpression);

    auto parameter = NodeMakerSingleton::getInstance().makeParameter("param", "int");
    BOOST_CHECK(parameter->getName() == "param");
    BOOST_CHECK(parameter->getType() == Types::INTEGER);

    auto autoVar = NodeMakerSingleton::getInstance().makeVariable("autoVar");
    auto intVar = NodeMakerSingleton::getInstance().makeVariable("intVar", "int");
    BOOST_CHECK(autoVar->getName() == "autoVar");
    BOOST_CHECK(autoVar->getType() == Types::AUTO);
    BOOST_CHECK(intVar->getName() == "intVar");
    BOOST_CHECK(intVar->getType() == Types::INTEGER);

    auto autoVarDecl = NodeMakerSingleton::getInstance().makeVariableDeclaration(autoVar);
    auto intVarDecl  = NodeMakerSingleton::getInstance().makeVariableDeclaration(intVar, leafInt);
    BOOST_CHECK(autoVarDecl->getDeclared() == autoVar);
    BOOST_CHECK(intVarDecl->getDeclared() == intVar);
    BOOST_CHECK(intVarDecl->getExpression() == leafInt);

    auto identifier = NodeMakerSingleton::getInstance().makeIdentifier("variable");
    BOOST_CHECK(identifier->getName() == "variable");

    auto assignment = NodeMakerSingleton::getInstance().makeAssignment("variable", leafInt);
    BOOST_CHECK(assignment->getIdentifier()->getName() == "variable");
    BOOST_CHECK(assignment->getExpression() == leafInt);

    auto declListLeaf = NodeMakerSingleton::getInstance().makeDeclarationListNode(autoVarDecl);
    BOOST_CHECK(declListLeaf->getContent() == autoVarDecl);

    auto declListList = NodeMakerSingleton::getInstance().makeDeclarationListNode(autoVarDecl, declListLeaf);
    BOOST_CHECK(declListList->getContent() == autoVarDecl);
    BOOST_CHECK(declListList->getNextNode() == declListLeaf);

    auto letIn = NodeMakerSingleton::getInstance().makeLetIn(declListList, binaryExpression);
    BOOST_CHECK(letIn->getDeclarationListNode() == declListList);
    BOOST_CHECK(letIn->getExpression() == binaryExpression);

    auto whileNode = NodeMakerSingleton::getInstance().makeWhile(leafInt, leafString);
    BOOST_CHECK(whileNode->getCondition() == leafInt);
    BOOST_CHECK(whileNode->getBody() == leafString);

    auto print = NodeMakerSingleton::getInstance().makePrint(leafInt);
    BOOST_CHECK(print->getExp() == leafInt);
    BOOST_CHECK(print->getType() == Types::INVALID);

    auto pair = NodeMakerSingleton::getInstance().makePairOfExpressions(leafInt, leafString);
    BOOST_CHECK(pair->getLeftExpression() == leafInt);
    BOOST_CHECK(pair->getRightExpression() == leafString);
}

BOOST_AUTO_TEST_SUITE_END()
