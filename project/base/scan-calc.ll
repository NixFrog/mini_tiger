/* Prologue  -*- C++ -*- */

%option noyywrap noinput nounput

%{
#include "parse-calc.hh"
yy::parser::location_type loc;

#define YY_USER_ACTION                          \
    loc.columns(yyleng);
%}

%%
  loc.step();

 // Rules.
"+"     return yy::parser::make_PLUS(loc);
"-"     return yy::parser::make_MINUS(loc);
"*"     return yy::parser::make_STAR(loc);
"/"     return yy::parser::make_SLASH(loc);
"=="    return yy::parser::make_EQUAL(loc);
"<>"    return yy::parser::make_NEQUAL(loc);
"<="    return yy::parser::make_LEQUAL(loc);
"<"     return yy::parser::make_LESSER(loc);
">="    return yy::parser::make_GEQUAL(loc);
">"     return yy::parser::make_GREATER(loc);
"&&"    return yy::parser::make_AND(loc);
"||"    return yy::parser::make_OR(loc);
"("     return yy::parser::make_LPAREN(loc);
")"     return yy::parser::make_RPAREN(loc);
"if"    return yy::parser::make_IF(loc);
"then"  return yy::parser::make_THEN(loc);
"else"  return yy::parser::make_ELSE(loc);
"let"   return yy::parser::make_LET(loc);
"var"   return yy::parser::make_VAR(loc);
":="    return yy::parser::make_ASSIGN(loc);
"in"    return yy::parser::make_IN(loc);
"end"   return yy::parser::make_END(loc);
";"     return yy::parser::make_EOT(loc);
","     return yy::parser::make_COMMA(loc);
"while" return yy::parser::make_WHILE(loc);
"do"    return yy::parser::make_DO(loc);
"for"   return yy::parser::make_FOR(loc);
"to"    return yy::parser::make_TO(loc);
"\""    return yy::parser::make_QUOTE(loc);
":"     return yy::parser::make_COLON(loc);
"function" return yy::parser::make_FUNCTION(loc);
"print" return yy::parser::make_PRINT(loc);

[0-9]+  return yy::parser::make_NUMBER(strtol(yytext, nullptr, 0), loc);
[a-zA-Z][a-zA-Z_0-9]* return yy::parser::make_STRING(yytext, loc);

[ \t]+  loc.step(); continue;
\n      loc.lines(yyleng); loc.step(); continue;

.   {
        std::cerr << "unexpected character: " << yytext << '\n';
        num_errors += 1;
    }

<<EOF>>  return yy::parser::make_EOF(loc);
%%
// Epilogue.
