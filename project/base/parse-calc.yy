// Prologue  -*- C++ -*-

// Generate C++.
%language "C++"

// Don't use unions, but variants, so that we can put objects in the
// stack.
%define api.value.type variant

// Require for make_FOO type-safe functions for all the token types.
%define api.token.constructor

// Prefix all the tokens in the implementation, to avoid clashes
// with, for instance, #define EOF.
%define api.token.prefix {TOK_}

// Verbose error messages.
%define parse.error verbose

// Enable yydebug.
%define parse.trace

// Enable location tracking.
%locations

// Let the parser "return" the last result.
%parse-param { int& result }

// Let the parser (and the scanner) exchange an argument with its
// caller: the number of errors.
%param { int& num_errors }

// Issue these declaration in the header file, after the definition
// of yy::parser.
%code provides
{
    // The declaration, for the scanner.
    #define YY_DECL yy::parser::symbol_type yylex(int& num_errors)

    // The declaration, for the parser.
    YY_DECL;
}

%code requires
{
    #include "../src/client_interfaces/ErrorManager.hh"
    #include "../src/client_interfaces/InputManager.hh"
    #include "../src/client_interfaces/NodeMaker.hh"
    #include <memory>
}

// Our tokens.
%token <int> NUMBER "number"
%token <std::string> STRING "identifier"
%token  PLUS    "+"
        MINUS   "-"
        STAR    "*"
        SLASH   "/"
        LPAREN  "("
        RPAREN  ")"
        EQUAL   "=="
        NEQUAL  "<>"
        LEQUAL  "<="
        LESSER  "<"
        GEQUAL  ">="
        GREATER ">"
        AND     "&&"
        OR      "||"
        IF      "if"
        THEN    "then"
        ELSE    "else"
        LET     "let"
        VAR     "var"
        ASSIGN  ":="
        IN      "in"
        END     "end"
        EOT     ";"
        COMMA   ","
        WHILE   "while"
        DO      "do"
        FOR     "for"
        TO      "to"
        QUOTE   "\""
        COLON   ":"
        FUNCTION "function"
        PRINT "print"
        EOF  0  "end-of-file"

// Our typed non terminals.
%nterm <std::shared_ptr<Kitten::Expression>> exp line binop arithmetic logic usage ifelse declMgt loops constant declaration declarationList variable parameter parameterList function print

// Better debug traces.
%printer { yyo << $$; } <int>;

// Precedence/associativity.
%right ","
%precedence IFX   //remove shift-reduce warning message to give if.then.else a higher priority than if alone
%precedence ELSE
%precedence ":="
%left     "||"
%left     "&&"
%nonassoc "==" "<>" "<" "<=" ">" ">="
%left     "+" "-"
%left     "*" "/"

%%
// Rules.
input:
    %empty
    |input line {
        if($2 != NULL && num_errors == 0){
            Kitten::InputManagerSingleton::getInstance().manageInput($2);
        }
        if(Kitten::InputManagerSingleton::getInstance().isPrompting())
        {
            num_errors = 0;
            Kitten::InputManagerSingleton::getInstance().updateInput();
        }
    }
    ;

line:
    exp end     { $$ = $1; }
    | error end { $$ = Kitten::NodeMakerSingleton::getInstance().makeVoidExpression(); }
    ;

end: EOT {} | EOF {} ;

exp:
    binop       { $$ = $1; }
    | usage     { $$ = $1; }
    | ifelse    { $$ = $1; }
    | declMgt   { $$ = $1; }
    | loops     { $$ = $1; }
    | constant  { $$ = $1; }
    | print     { $$ = $1; }
    ;

binop:
    arithmetic { $$ = $1; }
    | logic    { $$ = $1; }
    ;

arithmetic:
    exp "+" exp   { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Addition,         $1, $3); }
    | "+" exp     { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Addition,         Kitten::NodeMakerSingleton::getInstance().makeConstantLeaf<int>(0), $2); }
    | exp "-" exp { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Substraction,     $1, $3); }
    | "-" exp     { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Substraction,     Kitten::NodeMakerSingleton::getInstance().makeConstantLeaf<int>(0), $2); }
    | exp "*" exp { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Multiplication,   $1, $3); }
    | exp "/" exp { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Division,         $1, $3); }
    ;

print:
    "print" "(" exp ")" { $$ = Kitten::NodeMakerSingleton::getInstance().makePrint( $3 ); }
    ;

logic:
    exp "=="   exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Equality,         $1, $3); }
    | exp "<>" exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Inequality,       $1, $3); }
    | exp "<"  exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::LesserThan,       $1, $3); }
    | exp ">"  exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::GreaterThan,      $1, $3); }
    | exp "<=" exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::LesserOrEqualTo,  $1, $3); }
    | exp ">=" exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::GreaterOrEqualTo, $1, $3); }
    | exp "||" exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::Or,               $1, $3); }
    | exp "&&" exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeBinaryExpression( Kitten::BinaryExpression::And,              $1, $3); }
    ;

usage:
    exp "," exp     { $$ = Kitten::NodeMakerSingleton::getInstance().makePairOfExpressions($1, $3); }
    | "(" exp ")"   { $$ = $2; }
    | "(" error ")" { $$ = Kitten::NodeMakerSingleton::getInstance().makeVoidExpression(); yyerrok; }
    ;

ifelse:
    IF exp THEN exp %prec IFX   { $$ = Kitten::NodeMakerSingleton::getInstance().makeIfThenElse($2, $4, Kitten::NodeMakerSingleton::getInstance().makeVoidExpression());}
    | IF exp THEN exp ELSE exp  { $$ = Kitten::NodeMakerSingleton::getInstance().makeIfThenElse($2, $4, $6); }
    ;

declMgt:
    "let" declarationList "in" exp "end" { $$ = Kitten::NodeMakerSingleton::getInstance().makeLetIn($2, $4); }
    | "let" "in" exp "end"               { $$ = $3; }
    | STRING ":=" exp                    { $$ = Kitten::NodeMakerSingleton::getInstance().makeAssignment($1, $3);}
    ;

variable:
    STRING               { $$ = Kitten::NodeMakerSingleton::getInstance().makeVariable($1); }
    | STRING ":" STRING  { $$ = Kitten::NodeMakerSingleton::getInstance().makeVariable($1, $3); }
    ;

parameter:
    STRING ":" STRING { $$ = Kitten::NodeMakerSingleton::getInstance().makeParameter($1, $3); }
    ;

parameterList:
    parameter                     { $$ = Kitten::NodeMakerSingleton::getInstance().makeParameterListNode($1); }
    | parameter "," parameterList { $$ = Kitten::NodeMakerSingleton::getInstance().makeParameterListNode($1, $3); }
    ;

function:
    STRING "(" parameterList ")" ":" STRING ":=" "(" exp ")" { }
    ;

declaration:
    "var" variable ":=" exp { $$ = Kitten::NodeMakerSingleton::getInstance().makeVariableDeclaration($2, $4); }
    | "var" variable        { $$ = Kitten::NodeMakerSingleton::getInstance().makeVariableDeclaration($2); }
    | "function" function   { }
    ;

declarationList:
    declaration                   { $$ = Kitten::NodeMakerSingleton::getInstance().makeDeclarationListNode($1); }
    | declaration declarationList { $$ = Kitten::NodeMakerSingleton::getInstance().makeDeclarationListNode($1, $2); }
    | "(" declarationList ")"     { $$ = $2; }
    ;

loops:
    "while" exp "do" "(" exp ")"                        { $$ = Kitten::NodeMakerSingleton::getInstance().makeWhile($2, $5); }
    | "for" STRING ":=" exp "to" exp "do" "(" exp ")"   { $$ = Kitten::NodeMakerSingleton::getInstance().makeFor($2, $4, $6, $9); }
    ;

constant:
    "\"" STRING "\"" { $$ = Kitten::NodeMakerSingleton::getInstance().makeConstantLeaf<std::string>($2); }
    | NUMBER         { $$ = Kitten::NodeMakerSingleton::getInstance().makeConstantLeaf<int>($1); }
    | STRING         { $$ = Kitten::NodeMakerSingleton::getInstance().makeIdentifier($1); }
    ;
%%
// Epilogue.
void yy::parser::error(const location_type& loc, const std::string& s)
{
    num_errors += 1;
    std::cerr << loc << ": " << s << std::endl;
}

int main(int argc, char const* argv[])
{
    auto result = 0;
    auto num_errors = 0;

    Kitten::InputManagerSingleton::getInstance().manageOptions(argv, argv + argc);
    Kitten::InputManagerSingleton::getInstance().updateInput();

    yy::parser parser(result, num_errors);

    // Scanner debug traces.
    if (getenv("YYSCAN"))
    {
        extern int yy_flex_debug;
        yy_flex_debug = 1;
    }

    // Parser debug traces.
    if (getenv("YYDEBUG"))
    parser.set_debug_level(1);

    // 0 if success, 1 otherwise.
    auto status = parser.parse() || num_errors;

    return status || num_errors;
}
