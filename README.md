# Preview
This is Kitten, a small compiler made as a student project, inspired by Tiger's syntax.

# Building
Just make.
Note: if you do not want to create the test executable, use
```
make clean && make make_dirs && make calc
```

# Executing
The executables are generated in ./build/exec/*
It is possible to run any executable without parameters with "make run-<exec_name>".

The command "make run-tests" allows some comfort by piping the detailed results of the tests into tests/report/report.txt

The options available for execution are:

* calc: enters calculator mode. You can type whatever you want, typechecking happens every time you hit the enter key. To end coding, add ';' and press enter.
* calc -f <filename>: executes the specified filename. This forbids prompting.
* calc --no-pretty-print: forbids pretty printing
* calc --debug: forbids pretty printing and prompting


# Testing
Due to problems with the usage of the command test(), we decided to do the following:

* Make check allows to check that incorrect expressions are not accepted
* In project/examples/, all expressions in the file fail.kitty should fail, and it is possible to check by hand the results of the other tests.
* Although it is far from covering 100% of the code, some basic tests have been made with boost. They can assert that the creation of expression is correct, and that the results of evaluating integers are correct.

# Functionalities
For the moment, the compiler supports:

* Two basic types: int and string.
* Arithmetic operations, both binary and unary
* Comparisons ==, <>, <=, <, >=, >, ||, &&   ( '==' was chosen over '=' by personal preference )
* Basic compiler options
* Multiple input lines in command mode, ending a command with ';'. This also works in files.
* Expressions such as:

```
if exp1 then exp2 else exp3;
let var STRING := exp1 var STRING := exp2 in exp3 end;
while exp1 do ( exp2 );
for var STRING := exp1 to exp2 do ( exp3 );
exp1, exp2 ;
```

* Void nodes can replace an expression (i.e. if 1 then 2; becomes if 1 then 2 else VoidNode;)
* Parentheses are mandatory after a "do" keyword
* Affectation is supported
* Functions, but only in syntax. Printing a function makes the compiler crash.

# Bugs ( features ? ) and issues
* print prints both the last evaluated string and int, as Expressions themselves do not have a type member.
* functions make the compiler crash because they are not entirely coded yet.
* for loops require parentheses after "do". This is done in order to avoid ambiguity and allow the user to use several expressions in a loop.
* Expressions are always desugarized. This means that they are pretty-printed in an ugly fashion. It might have been wiser to use an observer that converts the base AST to a desugarized one.
* By checking the program with valgrind, there are 4 missing deletes and many read errors when parsing a test file.
* Due to a bad programmer, the program crashes if you call for a variable before declaring any. According to gdb and my knowledge, this is caused by accessing a statically-obtained member of scope that is not initialized. However, given the difficulty of writing the Scope class, this has not been fixed.
* The makefile does not recompile files using a templated class when its .tpp file is modified.